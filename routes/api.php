<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// URL: http://localhost/apirppc/public/api/v1/conexion

// DOCUMENTACION: http://localhost/apirppc/public/api/documentation


Route::group(['prefix' => 'v1', "middleware" => "apikey.validate"], function () {

        // VERIFICACIÓN DE LA CONEXIÓN A LA BASE DE DATOS ORACLE
        Route::get('conexion','apirppcController@conexion');

        // VERIFICACION DE FOLIO DEL TITULAR
        Route::get('titularfolio/{nu_folio}/{nom_tit}/{patern}/{matern}', 'apirppcController@VerificacionFolioTitular');

        // CONSULTA DE LOS DATOS DEL INMUEBLE
        Route::get('datosinmueble/{nu_folio}/{nu_ofic}', 'apirppcController@DatosDelInmueble');

});