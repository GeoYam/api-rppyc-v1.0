<?php

namespace App\Http\Middleware;

use Closure;

class ApiKeyValidate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      
      // BLOQUEAR ACCESO A LA INFORMACIÓN SI NO RECIBE LA API KEY

          if (!$request->has("api_key")) {
              return response()->json([
                'status' => 401,
                'message' => 'Acceso no autorizado',
              ], 401);
            }
      
      // HACE UNA COMPARACIÓN CON API KEY RECIBIDA CON LA ALMACENADA 
      
          if ($request->has("api_key")) {
            $api_key = "key_rppyc_query_fnPqT5xQEi5Vcb9wKwbCf65c3BjVGyBB";
            if ($request->input("api_key") != $api_key) {
              return response()->json([
                'status' => 401,
                'message' => 'Acceso no autorizado',
              ], 401);
            }
          }
      
          return $next($request);
    }
}
