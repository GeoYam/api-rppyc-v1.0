<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
* @OA\Info(title="API RPPyC", version="1.0")
*
* @OA\Server(url="http://localhost/apirppc/public", description="Host de manera local")
*
* @OA\SecurityScheme(
*   securityScheme="api_key",
*   type="apiKey",
*   in="query",
*   name="api_key"
* )
*/


class apirppcController extends Controller
{

    // FUNCIÓN DE VERIFICACIÓN DE CONEXIÓN
    public function conexion(){

        //$conexion = oci_connect('SYSTEM', 'root', '192.168.35.55:1521/rpp');

        $conexion = oci_new_connect('IQIT', 'IQ2019', '(DESCRIPTION = 
        (ADDRESS_LIST =
            (ADDRESS = (PROTOCOL = TCP)(Host= 10.9.35.155)(Port = 1521))
        ) 
        (CONNECT_DATA =
            (SID = playab)
            )
        )');

        if(!$conexion){
            $err = oci_error();
            echo $err['message'], "n";
            exit;
        } else {
            return response()->json(['Conexion'=>'Conexion establecida con Oracle'],200);
        }
    }



//--------------------------------------------------------------------------------------------------------------------------------------------


    //ANOTACIONES SWAGGER DE LA VERIFICACIÓN DE FOLIO DEL TITULAR

    /**
    * @OA\Get(
    *     path="/api/v1/titularfolio/{nu_folio}/{nom_tit}/{patern}/{matern}",
    *     summary="Verificar folio del titular",
    *     security={{"api_key":{}}},
    *     @OA\Parameter(
    *          name="nu_folio", description = "Folio de registro asignado por RPPyC", required=true, in="path"
    *     ),
    *     @OA\Parameter(
    *          name="nom_tit", description = "Nombre(s) del titular", required=true, in="path"
    *     ),
    *     @OA\Parameter(
    *          name="patern", description = "Apellido paterno del titular", required=true, in="path"
    *     ),
    *     @OA\Parameter(
    *          name="matern", description = "Apellido materno del titular", required=true, in="path"
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Se muestra mensaje indicando que el folio le pertenece al titular si los parametros son correctos 
    *         en caso contrario muestra mensaje que no le pertenece"
    *     )
    * )
    */


    //FUNCION PARA VERIFICAR QUE EL FOLIO Y NOMBRE EXISTAN

    public function VerificacionFolioTitular($nu_folio, $nom_tit, $patern, $matern){

        // Conexion
        $conexion = oci_new_connect('IQIT', 'IQ2019', '(DESCRIPTION = 
        (ADDRESS_LIST =
            (ADDRESS = (PROTOCOL = TCP)(Host= 10.9.35.155)(Port = 1521))
        ) 
        (CONNECT_DATA =
            (SID = playab)
            )
        )');

        if(!$conexion){
            $e = oci_error();
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        } 

        // Praparar la sentencia
        $consulta = "SELECT  propietarios.nom_tit, propietarios.patern, propietarios.matern, prop_inmob.nu_porcen
                    FROM inmobiliario
                    INNER JOIN (prop_inmob INNER JOIN propietarios ON propietarios.Id_prop = prop_inmob.Id_prop)
                    ON inmobiliario.nu_folio = prop_inmob.nu_folio AND inmobiliario.nu_ofic = prop_inmob.nu_ofic
                    WHERE inmobiliario.nu_folio= :nufolio AND propietarios.nom_tit= :nomtit 
                    AND propietarios.patern= :apepater AND propietarios.matern = :apemater ";

        $stid = oci_parse($conexion, $consulta);

        // Variable definido por el cliente en mayusculas
        $nomtit = strtoupper($nom_tit);
        $apellidopaterno = strtoupper($patern);
        $apellidomaterno = strtoupper($matern);

        // Asignación de la variable a la consulta
        oci_bind_by_name($stid, ':nufolio', $nu_folio);
        oci_bind_by_name($stid, ':nomtit', $nomtit);
        oci_bind_by_name($stid, ':apepater', $apellidopaterno);
        oci_bind_by_name($stid, ':apemater', $apellidomaterno);

        // Realizar la lógica de la cosulta
        $r = oci_execute($stid);
        if (!$r){
            $e = oci_error($stid);
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

        // Obtener los resultados de la consulta por columna
        $nrows = oci_fetch_all($stid, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
       
        // Imprimir los distintos tipos de respuesta según corresponda
        $vacio = count($res);

        if($vacio!=0){
            $consulta2 = "SELECT  propietarios.nom_tit, propietarios.patern, propietarios.matern, prop_inmob.nu_porcen
                    FROM inmobiliario
                    INNER JOIN (prop_inmob INNER JOIN propietarios ON propietarios.Id_prop = prop_inmob.Id_prop)
                    ON inmobiliario.nu_folio = prop_inmob.nu_folio AND inmobiliario.nu_ofic = prop_inmob.nu_ofic
                    WHERE inmobiliario.nu_folio= :nufolio";

            $stid2 = oci_parse($conexion, $consulta2);

            // Asignación de la variable a la consulta
            oci_bind_by_name($stid2, ':nufolio', $nu_folio);

            // Realizar la lógica de la cosulta
            $r = oci_execute($stid2);
            if (!$r){
                $e = oci_error($stid2);
                trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
            }

            // Obtener los resultados de la consulta por columna
            $nrows2 = oci_fetch_all($stid2, $res2, null, null, OCI_FETCHSTATEMENT_BY_ROW);

            
            return response()->json(['status'=>'El folio pertenece al titular','Propietarios'=>$res2], 200);
        }else{
            return response()->json(['status'=>'El folio no pertenece al titular'], 200); 
        }

        // Terminar función y conexión
        oci_free_statement($stid);
        oci_close($conexion);
    }



//--------------------------------------------------------------------------------------------------------------------------------------------



    //ANOTACIONES SWAGGER DE LA VERIFICACIÓN DE DATOS DEL INMUEBLE


    /**
    * @OA\Get(
    *     path="/api/v1/datosinmueble/{nu_folio}/{nu_ofic}",
    *     summary="Obtener datos del inmueble con el folio y número de oficina",
    *     security={{"api_key":{}}},
    *     @OA\Parameter(
    *          name="nu_folio", description ="Folio de registro asignado por RPPyC", required=true, in="path"
    *     ),
    *     @OA\Parameter(
    *          name="nu_ofic",description ="Número de oficina del registro", required=true, in="path"
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Se muestra datos del inmueble si los parametros son correctos 
    *         en caso contrario muestra mensaje de error"
    *     )
    * )
    */



    //FUNCION PARA VERIFICAR DATOS DEL INMUEBLE
    public function DatosDelInmueble($nu_folio, $nu_ofic){


        $id_estado=23;

        // Conexion
        $conexion = oci_new_connect('IQIT', 'IQ2019', '(DESCRIPTION = 
        (ADDRESS_LIST =
            (ADDRESS = (PROTOCOL = TCP)(Host= 10.9.35.155)(Port = 1521))
        ) 
        (CONNECT_DATA =
            (SID = playab)
            )
        )');

        if(!$conexion){
            $e = oci_error();
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        } 

        // Praparar la sentencia 
        $consulta = "SELECT  mov_inmob.acto
                    FROM inmobiliario
                    INNER JOIN (mov_inmob INNER JOIN tipo_acto ON tipo_acto.id_acto = mov_inmob.acto)
                    ON inmobiliario.nu_folio = mov_inmob.nu_folio AND inmobiliario.nu_ofic = mov_inmob.nu_ofic
                    INNER JOIN(municipios INNER JOIN estados ON estados.id_estado = municipios.id_estado)
                    ON municipios.id_municipio = inmobiliario.nu_munici 
                    WHERE inmobiliario.nu_folio= :nufolio AND inmobiliario.nu_ofic= :nuofic AND estados.id_estado = :idEstado  ORDER BY mov_inmob.fe_regist DESC";
          
        $stid = oci_parse($conexion, $consulta);

        // Asignación de la variable a la consulta
        oci_bind_by_name($stid, ':nufolio', $nu_folio);
        oci_bind_by_name($stid, ':nuofic', $nu_ofic);
        oci_bind_by_name($stid, ':idEstado', $id_estado);

        // Realizar la lógica de la cosulta
        $r = oci_execute($stid);
        if (!$r){
            $e = oci_error($stid);
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

        // Obtener el numero de resultados como un contador
        $nrows = oci_fetch_row($stid);


        // UNA VEZ TENIENDO EL [ACTO] MÁS RECIENTE SE SUSTITUYE EN LA CONSULTA DE DATOS
     
         
        if($nrows!=0){
            

                    // PREPARAMOS CONSULTA PARA [DATOS DE ESCRITURA]

                    // Praparar la sentencia
                    //consulta solo de los datos, sin agregar a todos los propietarios
                    /*$consultaEscritura = "SELECT  $nrows[0].num_escritura, $nrows[0].fec_escritura, $nrows[0].num_notaria,
                                        municipios.desc_municipio, estados.nom_estado, mov_inmob.fe_regist, inmobiliario.nu_folio
                                        FROM inmobiliario
                                        INNER JOIN (prop_inmob INNER JOIN propietarios ON propietarios.Id_prop = prop_inmob.Id_prop)
                                        ON inmobiliario.nu_folio = prop_inmob.nu_folio AND inmobiliario.nu_ofic = prop_inmob.nu_ofic
                                        INNER JOIN(municipios INNER JOIN estados ON estados.id_estado = municipios.id_estado)
                                        ON municipios.id_municipio = inmobiliario.nu_munici 
                                        INNER JOIN (mov_inmob INNER JOIN tipo_acto ON tipo_acto.id_acto = mov_inmob.acto)
                                        ON inmobiliario.nu_folio = mov_inmob.nu_folio AND inmobiliario.nu_ofic = mov_inmob.nu_ofic
                                        INNER JOIN $nrows[0] ON inmobiliario.nu_folio = $nrows[0].nu_folio AND inmobiliario.nu_ofic = $nrows[0].nu_ofic
                                        WHERE inmobiliario.nu_folio= :nufolio2 AND inmobiliario.nu_ofic= :nuofic2 AND estados.id_estado = :idEstado2  ORDER BY mov_inmob.fe_regist DESC ";*/

                    $consultaEscritura = "SELECT  $nrows[0].num_escritura, $nrows[0].fec_escritura, $nrows[0].num_notaria,
                                        municipios.desc_municipio, estados.nom_estado, mov_inmob.fe_regist, inmobiliario.nu_folio/*,
                                        //propietarios.nom_tit, propietarios.patern, propietarios.matern, prop_inmob.nu_porcen*/
                                        FROM inmobiliario
                                        INNER JOIN (prop_inmob INNER JOIN propietarios ON propietarios.Id_prop = prop_inmob.Id_prop)
                                        ON inmobiliario.nu_folio = prop_inmob.nu_folio AND inmobiliario.nu_ofic = prop_inmob.nu_ofic
                                        INNER JOIN(municipios INNER JOIN estados ON estados.id_estado = municipios.id_estado)
                                        ON municipios.id_municipio = inmobiliario.nu_munici 
                                        INNER JOIN (mov_inmob INNER JOIN tipo_acto ON tipo_acto.id_acto = mov_inmob.acto)
                                        ON inmobiliario.nu_folio = mov_inmob.nu_folio AND inmobiliario.nu_ofic = mov_inmob.nu_ofic
                                        INNER JOIN $nrows[0] ON inmobiliario.nu_folio = $nrows[0].nu_folio AND inmobiliario.nu_ofic = $nrows[0].nu_ofic
                                        WHERE inmobiliario.nu_folio= :nufolio2 AND inmobiliario.nu_ofic= :nuofic2 AND estados.id_estado = :idEstado2  ORDER BY mov_inmob.fe_regist DESC ";      


                                        $stidEscritura = oci_parse($conexion, $consultaEscritura); 

                                                // Asignación de la variable a la consulta
                                                oci_bind_by_name($stidEscritura, ':nufolio2', $nu_folio);
                                                oci_bind_by_name($stidEscritura, ':nuofic2', $nu_ofic);
                                                oci_bind_by_name($stidEscritura, ':idEstado2', $id_estado);

                                                // Realizar la lógica de la cosulta
                                                $p = oci_execute($stidEscritura);
                                                if (!$p){
                                                $q = oci_error($stidEscritura);
                                                trigger_error(htmlentities($q['message'], ENT_QUOTES), E_USER_ERROR);
                                                }

                                                // Obtener los resultados de la consulta por columna
                                                $nrows2 = oci_fetch_all($stidEscritura, $resEscritura, null, null, OCI_FETCHSTATEMENT_BY_ROW);
       
                                        if($nrows2!=0){
                                                return response()->json($resEscritura, 200);

                                        }else{
                                                return response()->json(['Error'=>'Folio no registrado en las oficinas, favor de registrarse en RPP'],200);
                                        }


        }else{
            return response()->json(['Error'=>'Folio no registrado en las oficinas, favor de registrarse en RPP'],200);
        }


        // Terminar función y conexión
        oci_free_statement($stid);
        oci_close($conexion);
    }
}